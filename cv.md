# Bruno Ramos

- [CV (PDF)](https://drive.google.com/file/d/1iH0V2rVcyJv4FL5o0UhATtTnp4AIze4L/view?usp=drive_link)
 
------

## Profile

I have 15+ years of experience as an Engineer/Architect/Manager, with hands-on experience in all levels of software development, such as database design, backend development, and systems integrations. Throughout my career, I have consistently demonstrated a hands-on approach and strategic vision to drive successful outcomes in dynamic environments.

Currently, I'm working as a Solutions Architect at the IWG Transformation Department, where I focus on crafting tailored solutions that effectively bridge complex business requirements with our technology roadmap and long-term strategy.

## Professional

### [2023 > now] > Solutions Architect at Transformation Department

In April 2023, I commenced my role as a Solution Architect within the IWG Plc Transformation team. My primary responsibility involves collaborating closely with business teams to devise and execute tailored solutions that align with their objectives. Operating within a team reporting directly to the CTO, I ensure our solutions adhere to the overarching IT strategy and landscape.

During my time at IWG, I:
* Led the selection process for a Shift Scheduling platform that could support the new staff management strategy outlined by our HR and Strategic teams.
* Led the selection of a Payment Orchestration platform to allow IWG to accept digital payments across all its locations, including credit cards and alternative payment methods, and to enable intelligent routing and fallback across multiple PSPs to reduce costs and improve customer experience.
* Defined a strategy to introduce the digital twin's concept at IWG to create a digital representation of all its spaces, furniture, and devices of all IWG centers.
* Participated in creating the business to adopt the Customer Data Platform, where I was responsible for high-level design, integration strategy, vendor assessment, and use case definition.
* Participate in technical discovery calls with stakeholders to gain an understanding of the current business issues, priorities and long term vision.
* Support in developing high level technical service design as part of the business ideation phase.
* Help business representatives prepare their cases for the IT Review and Categorisation calls with IT Senior Leadership.



### [2021 > 2023] Engineering Lead @ Covalo

In September 2021, I joined Covalo as Engineering Lead, responsible for insourcing all the development and growth of the engineering department. After the founders, I was the second person to join the company. Since the beginning, I have been part of the management team reporting directly to the CEO.
 
At Covalo, I was responsible for every aspect related to the Engineering and Product team. Being the most senior person in the company, I acted as a coach for the rest of the company, helping people grow in their roles, and exploring new approaches, ideas and techniques.
 
On the engineering side, I was accountable for the architecture, development, and operation of the Covalo platform, including database development, software architecture, API design, backend and frontend implementation, cloud infrastructure, integration with external partners, etc. 
 
On the product side, I worked closely with our Product Owner to ensure that the Product and Tech roadmaps were aligned with the company's strategic goals.

### [2016 > 2021] > Technical Manager/Solutions Architect 

During this period, I have worked as a technical manager, leading development teams, managing relationships with stakeholders and clients, and helping tech teams and businesses adopt agile approaches in enterprise environments.
 
I started moving towards a management career in 2016 when I was in charge of setting up and managing a development team responsible for developing a new product suite. I was responsible for the hiring process, coaching and mentoring the engineers, and leading the design and development of the product suite. During this period, I also worked closely with the management board setting the strategy, defining the products and services, and setting up the development team to address the business needs.
 
In 2019, I started working in a multinational group, Panalpina, as the leader of the Digital Integration Team. Acting as Team Leader/Line Manager, I was responsible for managing the corporate integration team and the relationship with the team's stakeholders and ensuring risk management and management report. During this time, I led the group through an agile transformation process and, following the takeover by DSV, through a merging process. I also worked as Integration Architect, helping the team to set up the first integration flows on Azure.
 
At Critical Software, as a Technical Manager/Solutions Architect, I was responsible for managing two teams in a digital transformation initiative in one of the largest financial groups in Portugal and assuming/ensuring the role of a delivery manager in a hybrid team of four members, helping the client coordinate a total of six delivery teams. I was also a member of the Technical Forum, ensuring that all technical decisions aligned with the client's requirements and the enterprise architecture blueprint.
 
In 2020, I joined Nuxeo as Team Manager of the nApps team, which owned the development of our customer experience for specific verticals, for example, Digital Asset Management. I was responsible for growing the team, driving technical choices, and aligning the product and technical roadmaps with Product Owners and Architects.

**During this period, my primary responsibilities were:**

* Managing/Leading development teams (recruitment, onboarding, coaching, performance reviews, training, etc) 
* Helping tech and business teams adopting agile approaches in enterprise environments
* Management Reporting
* Technical Advisory in different forums
* Managing the relationship with the clients’ technical and functional teams
* Promoting Tech Team’s relationship with different stakeholders, to gather feedback, ideas and communicate roadmap status
* Ensuring that overall technical solutions and architecture were aligned and based on the enterprise architecture guidelines and patterns
* Defining the architecture, design and implementation of the w2bill product suite (http://www.w2bill.com/) 
* Driving the strategic direction of products & services considering emerging and legacy technologies
* Facilitating technology and methodology decisions throughout the team
* Defining the architecture, design, and implementation of the first version of the Hydrokonekt IoT Platform that is currently managing more than 200k smart water devices in Antwerp (https://hydrokonekt.com/the-platform).

**Companies:**
* `[2020 > 2021]` Team Manager @ Nuxeo
* `[2020 > 2020]` Technical Manager/Solutions Architect @ Critical Software 
* `[2019 > 2020]` Team Leader of Integration Team/Integration Architect @ Panalpina/DSV 
* `[2016 > 2019]` Principal Engineer/Software Architect @ CMAS 


### [2008 > 2016] > Software Engineer/Architect 

I started my career as a Software Developer, continuously developing my skills in all software development life cycle phases, from planning to production. During this time, I worked in different business areas, such as Healthcare, Insurance, Finance, Media and Education, which helped me consolidate my technical background and ability to understand different business needs.

I consistently invested time learning and mastering the technologies I work with, reading and trying out different architectural patterns, designing solutions with clients' architects, and leading development teams. 

**During this period, my primary responsibilities were:**

* Helping clients to adopt new development techniques, tools, frameworks, and architectures
* Refactoring legacy applications, from the database to the user interface
* Designing, developing, and maintaining software applications
* Designing and implementing integrations with 3rd party software products/providers
* Managing technical contacts with software/service providers
* Leading small development teams 
* Coaching team members

**Companies:**

* `[2014 > 2016]` Software Engineer/Architect @ Polsarising
* `[2013 > 2014]` Software Engineer/Architect @ InformaDB 
* `[2012 > 2013]` Software Engineer @ KnowledgeWorks
* `[2009 > 2012]` Software Engineer @ Evolute 
* `[2008 > 2009]` Software Developer @ Sebasi 

